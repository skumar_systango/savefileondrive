//
//  ViewController.swift
//  SaveFileOnDrive
//
//  Created by macbook27 on 12/03/24.
//

import UIKit
import GoogleSignIn
import GoogleAPIClientForREST
import GTMSessionFetcher
import CommonCrypto


class ViewController: UIViewController {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtfEnterData: UITextField!
    
    let googleDriveService = GTLRDriveService()
    var googleUser: GIDGoogleUser?
    var uploadFolderID: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.uiDelegate = self
        googleDriveService.apiKey = "AIzaSyAma_502R4fVsHKmRR6FdYU5gzSfrJ8Wc8"
    }
    
    @IBAction func btnDownloadFile(_ sender: Any) {
        getFileAndDownload()
    }
    
    @IBAction func btnUploadFile(_ sender: Any) {
        createFolder(name: "SunilDrivefolder10", service: googleDriveService) { path in
            print(path)
        }
//        populateFolderID()
//        self.createFolder(
//            name: "myFolderName",
//            service: self.googleDriveService) {
////                self.uploadFolderID = $0
//            }
    }
    
    @IBAction func btnLogin(_ sender: Any) {
        login()
    }
    
    func login()  {
        if GIDSignIn.sharedInstance()?.currentUser != nil {
            GIDSignIn.sharedInstance()?.signOut()
//            initialise()
        } else {
            GIDSignIn.sharedInstance().delegate = self
//            GIDSignIn.sharedInstance()?.presentingViewController = self
            GIDSignIn.sharedInstance().scopes = [kGTLRAuthScopeDriveReadonly, kGTLRAuthScopeDrive]
            GIDSignIn.sharedInstance()?.signIn()
        }
    }
    
    
    func getFolderID(
        name: String,
        service: GTLRDriveService,
        user: GIDGoogleUser,
        completion: @escaping (String?) -> Void) {
        
        let query = GTLRDriveQuery_FilesList.query()

        // Comma-separated list of areas the search applies to. E.g., appDataFolder, photos, drive.
        query.spaces = "drive"
        
        // Comma-separated list of access levels to search in. Some possible values are "user,allTeamDrives" or "user"
        query.corpora = "user"
            
        let withName = "name = '\(name)'" // Case insensitive!
        let foldersOnly = "mimeType = 'application/vnd.google-apps.folder'"
        let ownedByUser = "'\(user.profile!.email!)' in owners"
        query.q = "\(withName) and \(foldersOnly) and \(ownedByUser)"
        
        service.executeQuery(query) { (_, result, error) in
            guard error == nil else {
                fatalError(error!.localizedDescription)
            }
                                     
            let folderList = result as! GTLRDrive_FileList

            // For brevity, assumes only one folder is returned.
            completion(folderList.files?.first?.identifier)
        }
    }
    
    
    func createFolder(
        name: String,
        service: GTLRDriveService,
        completion: @escaping (String) -> Void) {
        let folder = GTLRDrive_File()
        folder.mimeType = "application/vnd.google-apps.folder"
        folder.name = name
        // Google Drive folders are files with a special MIME-type.
        let query = GTLRDriveQuery_FilesCreate.query(withObject: folder, uploadParameters: nil)
        self.googleDriveService.executeQuery(query) { (_, file, error) in
            guard error == nil else {
                fatalError(error!.localizedDescription)
            }
            
            let folder = file as! GTLRDrive_File
//            let data = "ksabasbasbfkasbfkasbfkjasbfkasbfkasfb".data(using: .utf8)
            // Usage
            let pin = "YourPIN"
            let secretData = self.txtfEnterData.text ?? ""
            let salt = Data([0x00, 0x01, 0x02, 0x03]) // Salt should be randomly generated and stored securely
            let derivedKey = self.deriveKeyFromPassword(password: pin, salt: salt, keyLength: kCCKeySizeAES256)
            do {
                let encryptedData = try secretData.aesEncrypt(key: derivedKey, iv: derivedKey)
                let encryptedString = encryptedData.base64EncodedString()
                print("Encrypted string:", encryptedString)
                let decryptedString = try encryptedString.aesDecrypt(key: derivedKey, iv: derivedKey)
                print("Decrypted string:", decryptedString)
                self.uploadFile(name: folder.identifier ?? "", folderID: folder.identifier ?? "", data: encryptedData, mimeType: "text/plain", service: self.googleDriveService)
            } catch {
                print("Error:", error.localizedDescription)
            }
//         completion(folder.identifier!)
        }
    }
    
    func uploadFile(
        name: String,
        folderID: String,
        data: Data,
        mimeType: String,
        service: GTLRDriveService) {
        let file = GTLRDrive_File()
        file.name = "secrete.dpt"
        file.parents = [folderID]
        // Optionally, GTLRUploadParameters can also be created with a Data object.
        let uploadParameters = GTLRUploadParameters(data: data, mimeType: mimeType)
        
        let query = GTLRDriveQuery_FilesCreate.query(withObject: file, uploadParameters: uploadParameters)
        
        service.uploadProgressBlock = { _, totalBytesUploaded, totalBytesExpectedToUpload in
            // This block is called multiple times during upload and can
            // be used to update a progress indicator visible to the user.
        }
        service.executeQuery(query) { (_, result, error) in
            guard error == nil else {
                fatalError(error!.localizedDescription)
            }
            
            // Successful upload if no error is returned.
        }
    }
    
    
    func populateFolderID() {
        let myFolderName = "my-folder"
        if let user = googleUser {
            getFolderID(
                name: myFolderName,
                service: self.googleDriveService,
                user: user) { folderID in
                    if folderID == nil {
                      
                    } else {
                        // Folder already exists
                        self.uploadFolderID = folderID
                    }
                }
        }
        
    }
}


extension ViewController: GIDSignInDelegate, GIDSignInUIDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        // A nil error indicates a successful login
        if error == nil {
            // Include authorization headers/values with each Drive API request.
            self.googleDriveService.authorizer = user.authentication.fetcherAuthorizer()
            self.googleUser = user
        } else {
            self.googleDriveService.authorizer = nil
            self.googleUser = nil
        }
    }
    
    
    func deriveKeyFromPassword(password: String, salt: Data, keyLength: Int) -> Data {
        var derivedKey = [UInt8](repeating: 0, count: keyLength)
        password.withCString { passwordPtr in
            salt.withUnsafeBytes { saltPtr in
                CCKeyDerivationPBKDF(
                    CCPBKDFAlgorithm(kCCPBKDF2),
                    passwordPtr, password.utf8.count,
                    saltPtr.bindMemory(to: UInt8.self).baseAddress, salt.count,
                    CCPseudoRandomAlgorithm(kCCPRFHmacAlgSHA256),
                    10000, // Number of iterations
                    &derivedKey,
                    derivedKey.count
                )
            }
        }
        return Data(derivedKey)
    }
    public func search(_ name: String, onCompleted: @escaping (GTLRDrive_File?, Error?) -> ()) {
        let query = GTLRDriveQuery_FilesList.query()
        query.pageSize = 1
        query.q = "name contains 'secret'"
        self.googleDriveService.executeQuery(query) { (ticket, results, error) in
            onCompleted((results as? GTLRDrive_FileList)?.files?.first, error)
        }
    }

    public func download(_ fileItem: GTLRDrive_File, onCompleted: @escaping (Data?, Error?) -> ()) {
        guard let fileID = fileItem.identifier else {
            return onCompleted(nil, nil)
        }
        self.googleDriveService.executeQuery(GTLRDriveQuery_FilesGet.queryForMedia(withFileId: fileID)) { (ticket, file, error) in
            let objData = file as! GTLRDataObject
            print(objData)
            let pin = "YourPIN"
            let salt = Data([0x00, 0x01, 0x02, 0x03]) // Salt should be randomly generated and stored securely
            let derivedKey = self.deriveKeyFromPassword(password: pin, salt: salt, keyLength: kCCKeySizeAES256)
            let encryptedString = objData.data.base64EncodedString()
            print("Encrypted string:", encryptedString)
            do {
                let decryptedString = try encryptedString.aesDecrypt(key: derivedKey, iv: derivedKey)
                print("Decrypted string:", decryptedString)
                self.lblTitle.text = decryptedString
            } catch {
                print("error is \(error.localizedDescription)")
            }
       
//            guard let data = (file as? GTLRDataObject)?.data else {
//                return onCompleted(nil, nil)
//            }
            
//            onCompleted(data, nil)
        }
    }
    
    func getFileAndDownload()  {
        search("") { file, error in
            if let fetchFile = file {
                self.download(fetchFile) { data, error in
                     
                }
            }
           
        }
    }

}

extension String {
    func aesEncrypt(key: Data, iv: Data) throws -> Data {
        let data = self.data(using: .utf8)!
        var encryptedBytes = [UInt8](repeating: 0, count: data.count + kCCBlockSizeAES128)
        var encryptedLength: Int = 0
        
        let status = key.withUnsafeBytes { keyBytes in
            iv.withUnsafeBytes { ivBytes in
                data.withUnsafeBytes { dataBytes in
                    CCCrypt(
                        CCOperation(kCCEncrypt),
                        CCAlgorithm(kCCAlgorithmAES),
                        CCOptions(kCCOptionPKCS7Padding),
                        keyBytes.baseAddress, key.count,
                        ivBytes.baseAddress,
                        dataBytes.baseAddress, data.count,
                        &encryptedBytes,
                        encryptedBytes.count,
                        &encryptedLength
                    )
                }
            }
        }
        
        guard status == kCCSuccess else {
            throw NSError(domain: "encryptionError", code: Int(status), userInfo: nil)
        }
        
        return Data(bytes: encryptedBytes, count: encryptedLength)
    }
    
    func aesDecrypt(key: Data, iv: Data) throws -> String {
        let data = Data(base64Encoded: self)!
        var decryptedBytes = [UInt8](repeating: 0, count: data.count + kCCBlockSizeAES128)
        var decryptedLength: Int = 0
        
        let status = key.withUnsafeBytes { keyBytes in
            iv.withUnsafeBytes { ivBytes in
                data.withUnsafeBytes { dataBytes in
                    CCCrypt(
                        CCOperation(kCCDecrypt),
                        CCAlgorithm(kCCAlgorithmAES),
                        CCOptions(kCCOptionPKCS7Padding),
                        keyBytes.baseAddress, key.count,
                        ivBytes.baseAddress,
                        dataBytes.baseAddress, data.count,
                        &decryptedBytes,
                        decryptedBytes.count,
                        &decryptedLength
                    )
                }
            }
        }
        
        guard status == kCCSuccess else {
            throw NSError(domain: "decryptionError", code: Int(status), userInfo: nil)
        }
        
        return String(bytes: decryptedBytes.prefix(decryptedLength), encoding: .utf8)!
    }
    
    
    //    import GoogleAPIClientForREST
    //    import GTMSessionFetcher
    
    // Initialize the Drive API service
    //    let service = GTLRDriveService()
    //    service.authorizer = GTMAppAuthFetcherAuthorization(fromKeychainForName: "GDriveAuth")
    //
    //    ignore
    

}

    // Call the fetchSingleFileFromFolder function with the folder ID
//    let folderId = "your_folder_id_here"
//    fetchSingleFileFromFolder(folderId: folderId)
//}

